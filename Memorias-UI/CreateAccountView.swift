//
//  CreateAccountView.swift
//  Memorias
//
//  Created by Laura Gonzalez on 2021-02-22.
//

import SwiftUI


struct CreateAccountView: View {
    
    init(){
        UITableView.appearance().backgroundColor = .clear
        
        
    }
    

    @State private var username: String = ""
    @State private var password: String = ""
    @State private var name: String = ""
    
    enum genders: String, CaseIterable {
        case male, female, other
    }

    @State private var gender: genders = .other
    
    var body: some View {
        //let periwinkle : Color = Color(red: 94, green: 94, blue: 196)
        
        VStack(alignment: .center) {
            
            //Spacer()
            
            //Welcome text
            Text("Sign-up").foregroundColor(.black)
                .font(.custom("Questrial-Regular", size: 40))
                .padding(.top, 170)
                .padding(.bottom, 20)
            
 
                
            //Name
            HStack{
                Text("First name: ")
                    .font(.custom("Lato-Regular", size: 27))
                    .padding(.horizontal, 30)
                
                TextField("Your first name",
                          text: $name)
                    .padding(10)
                    .font(.custom("Lato-Regular", size: 27))
                    .frame(width: .none, height: 60, alignment: .center)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 2))
                    .padding(.leading, 90)
                                       
            }.padding(.bottom, 30)
            
            //Name
            HStack{
                Text("Last name: ")
                    .font(.custom("Lato-Regular", size: 27))
                    .padding(.horizontal, 30)
                
                TextField("Your last name",
                          text: $name)
                    .padding(10)
                    .font(.custom("Lato-Regular", size: 27))
                    .frame(width: .none, height: 60, alignment: .center)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 2))
                    .padding(.leading, 92)
            }.padding(.bottom, 30)
                
            
            //Username (Your Email)
            HStack{
                Text("Email: ")
                    .font(.custom("Lato-Regular", size: 27))
                    .padding(.horizontal, 30)
                
                TextField("Your email",
                          text: $username)
                    .padding(10)
                    .font(.custom("Lato-Regular", size: 27))
                    .frame(width: .none, height: 60, alignment: .center)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 2))
                    .padding(.leading, 150)
            }.padding(.bottom, 30)
            
            //Username
            HStack{
                Text("Username: ")
                    .font(.custom("Lato-Regular", size: 27))
                    .padding(.horizontal, 30)
                
                TextField("Your username",
                          text: $name)
                    .padding(10)
                    .font(.custom("Lato-Regular", size: 27))
                    .frame(width: .none, height: 60, alignment: .center)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 2))
                    .padding(.leading, 93)
            }.padding(.bottom, 30)
            
            
            //Password
            HStack{
                Text("Password: ")
                    .font(.custom("Lato-Regular", size: 27))
                    .padding(.horizontal, 30)
                
                TextField("Password",
                    text: $password)
                    .padding(10)
                    .font(.custom("Lato-Regular", size: 27))
                    .frame(width: .none, height: 60, alignment: .center)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 2))
                    .padding(.leading, 99)
                    
            }.padding(.bottom, 30)
                       
            
            //Password
            HStack{
                Text("Confirm password: ")
                    .font(.custom("Lato-Regular", size: 27))
                    .padding(.horizontal, 30)
                
                TextField("Re-enter your password",
                    text: $password)
                    .padding(10)
                    .font(.custom("Lato-Regular", size: 27))
                    .frame(width: .none, height: 60, alignment: .center)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.black, lineWidth: 2))
                   
                    
            }.padding(.bottom, 30)
            
            
            //Signup Button
            Button(action: {
                print("Signup button clicked")

                
                /*if Amplify.Auth.getCurrentUser() == nil {
                    sessionManager.createAccountError = true
                }*/
                
            }) {
                Text("  Create Account  ")
                    .font(.custom("Lato-Regular", size: 28))
                    .padding(20)
                    .padding(.horizontal, 130)
                    .background(Color("Periwinkle"))
                    .cornerRadius(10)
                    .foregroundColor(Color.white)
                    .padding(.top, 10)
            }
            
            LabelledDivider(label: "or")
            
            
            
           
            Button("Already have an account? Login here", action: fake)
                .font(.custom("Lato-Regular", size: 22))
                .padding(.top, 40)
                .padding(.bottom, 70)
                
             
            //Spacer()
            
            
        }.padding(.horizontal, 30).background(
            Image("Ipad - Background").resizable().scaledToFill())


    }
    func fake(){
        
    }
    
}

struct CreateAccountView_Previews: PreviewProvider {
    static var previews: some View {
        CreateAccountView()
    }
}

struct LabelledDivider: View {

    let label: String
    let horizontalPadding: CGFloat
    let color: Color

    init(label: String, horizontalPadding: CGFloat = 20, color: Color = .black) {
        self.label = label
        self.horizontalPadding = horizontalPadding
        self.color = color
    }

    var body: some View {
        HStack {
            line
            Text(label).foregroundColor(color).font(.custom("Lato-Regular", size: 26))
            line
        }
    }

    var line: some View {
        VStack { Divider().background(color) }.padding(horizontalPadding)
    }
}




