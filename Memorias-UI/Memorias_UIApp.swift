//
//  Memorias_UIApp.swift
//  Memorias-UI
//
//  Created by Laura Gonzalez on 2021-06-04.
//

import SwiftUI

@main
struct Memorias_UIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
